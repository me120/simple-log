module.exports = loggingId => {
  global.msg = (message, preventNewLine) => {
    process.stdout.write(`                                                                              \r` +
                         `\x1b[1m\x1b[32m${loggingId}\x1b[0m: ${message}${preventNewLine ? "\r" : "\n"}`);
  };
  
  global.wrn = (warning, preventNewLine) => {
    process.stdout.write(`                                                                              \r` +
                         `\x1b[1m\x1b[33m${loggingId}\x1b[0m: ${warning}${preventNewLine ? "\r" : "\n"}`);
  };
  
  global.err = (error, preventNewLine) => {
    process.stdout.write(`                                                                              \r` +
                         `\x1b[1m\x1b[31m${loggingId}\x1b[0m: ${error}${preventNewLine ? "\r" : "\n"}`);
  };
};